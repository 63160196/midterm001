import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useOrderStore = defineStore("order", () => {
  const functionChoose = ref(0);

  //   const doubleCount = computed(() => count.value * 2);

  function choose(funcIndex: number) {
    functionChoose.value = funcIndex;
  }

  return { choose, functionChoose };
});
